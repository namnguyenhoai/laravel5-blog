@extends('layouts.master')

@section('head.title')
Home Page
@endsection

@section('body.content')
<div class="container">
	@foreach ($articles as $article)
	<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<h2>{{ $article->title }}</h2>
				<div>{{ $article->content }}</div>
				<a href="{{ route('article.show', $article->id) }}">Read more</a>
			</div>
	</div>
	@endforeach
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			{!! $articles->render() !!}
		</div>
	</div>
</div>
@endsection