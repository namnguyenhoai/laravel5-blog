<div class="form-group">
	{!! Form::label('title', 'Article Title', array('class' => 'control-label')) !!}
	{!! Form::text('title', null, array('id' => 'title', 'class' => 'form-control', 'placeholder' => 'Article title', 'required' => true)) !!}
</div>

<div class="form-group">
	{!! Form::label('title', 'Article Content', ['class' => 'control-label']) !!}
	{!! Form::text('content', null, array('id' => 'content', 'class' => 'form-control', 'placeholder' => 'Article Content', 'required' => true)) !!}
</div>

<div class="form-group">
	{!! Form::submit($button_name, ['class' => 'btn btn-primary']) !!}
</div>