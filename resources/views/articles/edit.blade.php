@extends('layouts.master')

@section('head.title')
Edit article
@endsection

@section('body.content')
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<h1>Edit article</h1>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			
			{!! Form::model($article, array(
				'route' => ['articles.update', $article->id],
				'method' => 'put',
				'class' => 'form-horizontal'
				))
			!!}
			
			@include('articles._form', ['button_name' => 'Update'])
	
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection