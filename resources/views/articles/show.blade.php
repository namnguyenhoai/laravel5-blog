@extends('layouts.master')

@section('head.title')
{{ $article->title }}
@endsection

@section('body.content')
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<a href="{{ url('/') }}" class="btn btn-link"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<h2>{{ $article->title }}</h2>
				<div>{{ $article->content }}</div>
			</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<a href="{{ route('articles.edit', $article->id) }}" class="btn btn-info">Edit</a>
			{!! Form::open([
				'route' => ['articles.destroy', $article->id],
				'method' => 'DELETE',
				'class' => 'inline',
				'style' => 'display:inline'
				]) 
			!!}
			<button class="btn btn-danger">Delete</button>

			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection