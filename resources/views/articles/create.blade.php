@extends('layouts.master')

@section('head.title')
Create new blog
@endsection

@section('body.content')
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<h1>Create new article</h1>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			<!--<form action=" {{ route('articles.store') }}" method="POST" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<label for="title" class="control-label">Title</label>
					<input type="text" class="form-control" name="title" id="title" required placeholder="Article Title">
				</div>

				<div class="form-group">
					<label for="content" class="control-label">Content</label>
					<input type="text" class="form-control" id="content" name="content" required placeholder="Article Conent">
				</div>

				<div class="form-group">
					<button class="btn btn-primary">Add New</button>
				</div>
			</form>-->

			{!! Form::open(array(
				'route' => 'articles.store',
				'method' => 'post',
				'class' => 'form-horizontal'
				))
			!!}
			
			@include('articles._form', ['button_name' => 'Create'])
	
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection