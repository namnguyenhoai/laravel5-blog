<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';

    // cho phep cac truong sau duoc dua du lieu vao
    protected $fillable = [
    	'title',
    	'content'
    ];
}
