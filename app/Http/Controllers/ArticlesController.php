<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Http\Requests\ArticleFormRequest;

use App\Article;



class ArticlesController extends Controller
{
    public function index()
    {
    	$articles = Article::paginate(4);

    	return view('articles.index', compact('articles'));
	}

	public function show($id)
	{
		$article = Article::find($id);
		// hoac dung $article = Article::where('id', $id)->first();
		return view('articles.show', compact('article')); 
		//hoac dung return view('article')->with('article', $article);
	}

	public function create()
	{
		return view('articles.create'); // trong folder view\article co file create.blade.php
	}

	public function store(ArticleFormRequest $request)
	{
		//dd(Input::get('title')); // die() and dump(): dung chuong trinh va in ra ket qua
		$title = $request->input('title'); //neu co $request roi thi ko dung Input nua Input::get('title');
		$content = $request->input('content'); //Input::get('content');

		// store in database
		Article::create([
			'title' => $title,
			'content' => $content	
			]);

		// return to article list
		return redirect()->route('articles.index');
	}

	public function edit($id)
	{
		$article = Article::find($id); // cach nay nguy hiem neu co 2 IDs
		return view('articles.edit', compact('article'));
	}

	public function update($id, ArticleFormRequest $request)
	{
		//dd($id);
		$article = Article::find($id); // cach nay nguy hiem neu co 2 IDs

		$article->update([
			'title' => $request->get('title'),
			'content' => $request->get('content')
			]);

		// return to article list
		return redirect()->route('articles.index');
	}

	public function destroy($id)
	{
		$article = Article::find($id); // cach nay nguy hiem neu co 2 IDs
		$article->delete();

		return redirect()->route('articles.index');
	}
}
